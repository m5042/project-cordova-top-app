# Web Mobile | Projet Cordova

###### Author : [Enzo Avagliano](https://gitlab.com/EloxFire)

###### Version : 1.0.0



#### Sujet :

Réalisation d'une application de création de top :

- L'utilisateur a une page d'accueil avec la liste des tops créés
- Lorsque l'utilisateur clique sur un item de la liste, j'ai une vue du top
- Avoir un bouton de création d'un top
- Formulaire de création d'un top avec un titre et une liste de sujets

___



#### Installation

Clonez le repertoire Git :

```bash
git clone git@gitlab.com:m5042/project-cordova-top-app.git
```



Ouvrez et configuez le projet (linux) :

```bash
cd project-cordova-top-app #Placement dans le repertoire du projet
npm i #Installation des dépendances
cordova platform add browser #Ajout de la plateforme (si ce n'est pas déjà fait)
cordova build browser #Build de l'application
```



Lancez le projet :

```bash
cordova run browser #Démarrage de l'application
```



#### Plugins Cordova utilisés :

- OneSignal (ne marche pas...)
- Cordova Device
- Cordova Status Bar



#### Autres outils  :

- Bootstrap
- PopperJS
- JQuery



##### Si vous avez des questions : enzo.avagliano@ynov.com

##### ![](https://media.discordapp.net/attachments/814250385190420483/905820993816256562/brand.png?width=1440&height=375)

